# -*- coding: utf-8 -*-
"""
Created on Tue Oct 26 13:28:38 2021

@author: Seth Yakel
"""

''' @file                   controller.py
    @brief
    
    Have a share for gain
    
    do closed loop and motor stuff in same task
    
    @details
    @author                 Seth Yakel
    @author                 Nicole Croft
    @date                   26 October 2021
'''

class ClosedLoop:
    
    def __init__(self):
        ''' @brief          Completes setup and initializes the appropriate
                            parameters
            @details
        '''
        
        
        pass
    
    def update(self, ref_value, meas_value):
        ''' @brief          Computes and returns the actuation value based on
                            the measured and reference values
            @details
        '''
        self.error = ref_value - meas_value
        
        self.actuation = self.error * self.Kp 
        
        self.actuation = float(self.actuation)
        
        return self.actuation
        
        pass
    
    def set_Kp(self, value):
        ''' @brief          Sets the calculated Kp value
                            
            @details
        '''
        self.Kp = value
        
        pass