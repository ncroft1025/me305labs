'''@file                    task_user.py
   @brief                   Constructs the finite state machine for user_task
   @details                 User_task includes three functions: one to initialize appropriate
                            variables, one to carry out appropriate actions within each state
                            and the proper movement between them, and one to
                            carry out the transition between each of the states. The final function
                            is not necessary, but makes the code more readable and user-friendly.
                            See description of the self.transition_to function for more details.
                            
                            The user task has 6 disticnt states and has the following task diagram:
                                
                            \image html task_user_states.PNG "User Task State Diagram"
                            
                            
   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    October 19, 2021
'''

import pyb, utime
import time
import array as arr
import math
from micropython import const

## @brief A variable representing state 0
#  @details This state is the initialization state
#
S0_INIT                 = const(0)

## @brief A variable representing state 1
#  @details This state prompts the user and waits for an input
#
S1_WAIT_FOR_INPUT       = const(1)

## @brief A variable representing state 2
#  @details This state triggers the flag variable and resets the enc_pos and delta to zero
#
S2_ZERO_POS             = const(2)

## @brief A variable representing state 3
#  @details This state reads and prints the most recent enc_pos value
#
S3_PRINT_POS            = const(3)

## @brief A variable representing state 4
#  @details This state reads and prints the most recent delta value
#
S4_PRINT_DELTA          = const(4)

## @brief A variable representing state 5
#  @details This state collects encoder position data for 30 seconds and looks for an input of 's' or 'S' from the user. It prints the data at the end of the 30 seconds
#
S5_COLLECT              = const(5)

## @brief A variable representing state 6
#  @details This state is transitioned to when state 5 is ended early and prints what has been collected
#
S6_END_COLLECT          = const(6)

S7_ZERO_POS_ENC2        = const(7)

S8_PRINT_POS_ENC2       = const(8)

S9_PRINT_DELTA_ENC2     = const(9)

S10_COLLECT_ENC2        = const(10)

S11_END_COLLECT_ENC2    = const(11)

S12_SET_DUTY_CYCLE      = const(12)

S13_SET_DUTY_CYCLE_ENC2      = const(13)

S14_STEP_RESPONSE_INPUT = const(14)

S15_STEP_RESPONSE_DATA_COLLECT     = const(15)

S16_END_STEP_COLLECT               = const(16)

S17_STEP_RESPONSE_INPUT_2 = const(17)

S18_STEP_RESPONSE_DATA_COLLECT_2 = const(18)

S19_END_STEP_COLLECT_2 = const(19)

S20_CLEAR_FAULT_1 = const(20)

S21_CLEAR_FAULT_2 = const(21)





class Task_User:
    ''' @brief              Defines the Task_User class
        @details            The class contains three functions: init, run, and transition_to.
                            The class defines appropriate variables and maps the prompts and
                            actions required by the task_user finite state machine.
    '''

    # Define the constructor
    def __init__(self, period, delta_1, enc_pos_1, flag_1, delta_2, enc_pos_2, flag_2, loop_mode, loop_mode_2, switch_1, switch_2, K_p, K_p_2, omega_ref_1, omega_ref_2, PWM_1, PWM_2, motor_1, motor_2):
        ''' @brief          Constructs an user task object
            @details        The init function initializes the state machine to the S0 INIT state,
                            initializes runs of the state machine to zero, and defines the serial
                            import so that the file can read user imports from the keyboard to
                            transition between states.
        '''

        ## The period of the task (in ms)
        self.period = period

        ## A shares.Share object representing encoder position
        self.enc_pos_1 = enc_pos_1
        
        ## A shares.Share object representing encoder position
        self.enc_pos_2 = enc_pos_2
        
        ## A shares.Share object representing encoder position
        self.delta_1 = delta_1

        ## A shares.Share object representing change in encoder position
        self.delta_2 = delta_2

        ## The state to run in the finte state machine
        self.state = S0_INIT

        ## Number of runs of the finite state machine
        self.runs = 0

        ## Serial port for user input
        self.serport = pyb.USB_VCP()

        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
        
        ## @brief A time variable
        #  @details Used to loop through the collection function
        #
        self.time_ = 0
        
        ## @brief Variable used in a time.sleep function
        #  @details Sets the amount of time which will be delayed
        #
        self.time_step = 1
        
        ## @brief A shared trigger variable used in between task_encoder and task_user
        #  @details Used to zero the position of the encoder by switching from low to high
        #
        self.flag_1 = flag_1
        
        self.flag_2 = flag_2
        
        self.counter_ENC1 = 0
        
        self.counter_ENC2 = 0
        
        self.counter_Step_1 = 0
        
        self.counter_Step_2 = 0
        
        self._time_array_1 = arr.array('f',1500*[0])
        
        #self._time_array_2 = arr.array('f',1500*[0])
        
        self._pos_array_1 = arr.array('f',1500*[0])
        
        #self._pos_array_2 = arr.array('f',1500*[0])
        
        self._omega_array_1 = arr.array('f',1500*[0])
        
        #self._omega_array_2 = arr.array('f',1500*[0])

        self.omega_ref_1 = omega_ref_1
        
        self.omega_ref_2 = omega_ref_2
        
        self.switch_1 = switch_1
        
        self.switch_2 = switch_2
        
        self.PWM_1 = PWM_1
        
        self.PWM_2 = PWM_2
        
        self.loop_mode_1 = loop_mode
        
        self.loop_mode_2 = loop_mode_2
        
        self._step_time_array_1 = arr.array('f',501*[0])
        
        self._step_omega_array_1 = arr.array('f',501*[0])
        
        self.motor_1 = motor_1
        
        self.motor_2 = motor_2
        
        self.K_p = K_p
        
        self.K_p_2 = K_p_2
        
        
        
        

    def run(self):
        ''' @brief          Constructs the flow of the user_task finite state machine
            @details        The user_task finite state machine has seven states: S0-S6.
                            This function runs each of these states and defines the proper
                            transitions between them according to the state diagram.
                            The serial import variable defined in the init function is utilized
                            so that user inputs can be interpreted and prompt the function to
                            transition between the states.
                            S0: INIT
                            S1: WAIT FOR INPUT
                            S2: ZERO POSITION
                            S3: PRINT POSITION
                            S4: PRINT DELTA
                            S5: COLLECT DATA FOR 30 SECONDS AND PRINT RESULTS
                            S6: END DATA COLLECTION PREMATURELY
        '''
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if (self.state==0):
                print('Welcome. Initial position of the encoder is zero.')
                print('Choose from list of input options: ')
                print('-To print the position of the encoder, press "p"')
                print('-To print delta between the last two positions, press "d"')
                print('-To re-zero the position of the encoder, press "z"')
                print('-To collect position data for 30 seconds, press "g"')
                print('-To pre-emptively terminate data collection, press "s"')
                print('-To set a duty cycle for motor 1, press m')
                print('-Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)

            elif (self.state==1):
                if (self.serport.any()):
                    
                    ## @brief Variable used to prompt the input of a command by the user
                    #  @details Used with the serport.read() function
                    #
                    self.prompt = self.serport.read(1)
                    if self.prompt == b'z':
                        self.transition_to(S2_ZERO_POS)
                    elif self.prompt == b'Z':
                        self.transition_to(S7_ZERO_POS_ENC2)
                    elif self.prompt == b'p':
                        self.transition_to(S3_PRINT_POS)
                    elif self.prompt == b'P':
                        self.transition_to(S8_PRINT_POS_ENC2)
                    elif self.prompt == b'd':
                        self.transition_to(S4_PRINT_DELTA)
                    elif self.prompt == b'D':
                        self.transition_to(S9_PRINT_DELTA_ENC2)
                    elif self.prompt == b'm':
                        self.transition_to(S12_SET_DUTY_CYCLE)
                    elif self.prompt == b'M':
                        self.transition_to(S13_SET_DUTY_CYCLE_ENC2)
                    elif self.prompt == b'g':
                        self.transition_to(S5_COLLECT)
                    elif self.prompt == b'G':
                        self.transition_to(S10_COLLECT_ENC2)
                    elif self.prompt == b'v':
                        self.transition_to(S14_STEP_RESPONSE_INPUT)
                    elif self.prompt == b'V':
                        self.transition_to(S17_STEP_RESPONSE_INPUT_2)
                    elif self.prompt == b'c':
                        self.transition_to(S20_CLEAR_FAULT_1)
                        
                    else:
                        print('Command \'{:}\' is invalid.'.format(self.prompt))
                        print('Waiting for user input: ')

            elif (self.state==2):
                self.flag_1.write(1)
                print('Encoder 1 position zeroed.')
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)
                
            elif (self.state==7):
                self.flag_2.write(1)
                print('Encoder 2 position zeroed.')
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)

            elif (self.state==3):
                print('Encoder 1 position is: {:}'.format(self.enc_pos_1.read()))
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)
                
            elif (self.state==8):
                print('Encoder 2 position is: {:}'.format(self.enc_pos_2.read()))
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)

            elif (self.state==4):
                print('Encoder 1 Delta is: {:3f}'.format(self.delta_1.read()))
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)
                
            elif (self.state==9):
                print('Encoder 2 Delta is: {:3f}'.format(self.delta_2.read()))
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)
                
            elif (self.state==12):
                duty1 = input('Please enter a duty cycle for motor 1:')
                
                duty1 = float(duty1)
                
                self.PWM_1.write(duty1)
                
                self.loop_mode_1.write(1)
                
                self.transition_to(S1_WAIT_FOR_INPUT)
                
            elif (self.state==13):
                duty2 = input('Please enter a duty cycle for motor 2:')
                
                duty2 = float(duty2)
                
                self.PWM_2.write(duty2)
                
                self.loop_mode_2.write(1)
                
                self.transition_to(S1_WAIT_FOR_INPUT)

            elif (self.state==5):
                
                print('Collecting data points {:}/1500...'.format(self.counter_ENC1))
                
                if (self.serport.any()):
                    self.prompt = self.serport.read(1)
                    if self.prompt == b's':
                        self.transition_to(S6_END_COLLECT)
                    else:
                        pass
                
                if self.counter_ENC1 > 1499:
                    self.transition_to(S6_END_COLLECT)
                
                
                else:
                    self._time_array_1[self.counter_ENC1] = self.counter_ENC1*.02
                
                    self._pos_array_1[self.counter_ENC1] = self.enc_pos_1.read()*math.pi/2000
                
                    self._omega_array_1[self.counter_ENC1] = self.delta_1.read()             
                    time.sleep(.02)
                
                    self.counter_ENC1 += 1
                
                
                
            elif (self.state==10):
                
                print('Collecting data points {:}/1500...'.format(self.counter_ENC2))
                
                if (self.serport.any()):
                    self.prompt = self.serport.read(1)
                    if self.prompt == b's':
                        self.transition_to(S6_END_COLLECT)
                    else:
                        pass
                
                if self.counter_ENC2 > 1499:
                    self.transition_to(S6_END_COLLECT)
                
                else:
                    
                    self._time_array_2[self.counter_ENC2] = self.counter_ENC2*.02
                
                    self._pos_array_2[self.counter_ENC2] = self.enc_pos_2.read()*math.pi/2000
                
                    self._omega_array_2[self.counter_ENC2] = self.delta_2.read()
                
                    time.sleep(.02)
                
                    self.counter_ENC2 += 1
                
                        
            elif (self.state==6):
                print('Data collection ended.')
                
                for idx in range (0, self.counter_ENC1):
                    print('{:10.2f}, {:10.3f}, {:10.3f}'.format(self._time_array_1[idx],self._pos_array_1[idx],self._omega_array_1[idx]))
                
                self.counter_ENC1 = 0
                print('Waiting for user input: ')    
                self.transition_to(S1_WAIT_FOR_INPUT)


            elif (self.state==11):
                print('Data collection ended.')
                
                for idx in range (0, self.counter_ENC2):
                    print('{:10.2f}, {:10.3f}, {:10.3f}'.format(self._time_array_2[idx],self._pos_array_2[idx],self._omega_array_2[idx]))
                
                self.counter_ENC2 = 0
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)
                
            elif (self.state==14):
                
                K_p = input ('Please enter a proportional gain for motor 1:')
                
                K_p = float(K_p)
                
                velo = input('Please enter a velocity setpoint for motor 1:')
                
                velo = float(velo)
                
                self.K_p.write(K_p)
                
                self.omega_ref_1.write(velo)
                
                
                self.transition_to(S15_STEP_RESPONSE_DATA_COLLECT)
                
                self.loop_mode_1.write(2)
                
                
            elif (self.state==15):
                
                print('Collecting data points {:}/500...'.format(self.counter_Step_1))
                
                
                if (self.serport.any()):
                    self.prompt = self.serport.read(1)
                    if self.prompt == b's':
                        self.switch_1.write(1)
                        self.transition_to(S16_END_STEP_COLLECT)
                    else:
                        pass
                
                if self.counter_Step_1 > 500:
                    self.switch_1.write(1)
                    self.transition_to(S16_END_STEP_COLLECT)
                    
                else:
                    

                    self._step_time_array_1[self.counter_Step_1] = self.counter_Step_1*.02
                
                    self._step_omega_array_1[self.counter_Step_1] = (self.delta_1.read())
                
                    time.sleep(.02)
                
                    self.counter_Step_1 += 1
                
            elif (self.state==16):
                
                print('Data collection ended.')
                
                for idx in range (0, self.counter_Step_1):
                    print('{:10.2f}, {:10.3f}'.format(self._step_time_array_1[idx],self._step_omega_array_1[idx]))
                
                self.counter_Step_1 = 0
                
                
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)
                
                
            elif (self.state==17):
                
                K_p = input ('Please enter a proportional gain for motor 2:')
                
                K_p = float(K_p)
                
                velo = input('Please enter a velocity setpoint for motor 2:')
                
                velo = float(velo)
                
                self.K_p_2.write(K_p)
                
                self.omega_ref_2.write(velo)
                
                
                self.transition_to(S18_STEP_RESPONSE_DATA_COLLECT_2)
                
                self.loop_mode_2.write(2)
            
            elif (self.state==18):
                
                print('Collecting data points {:}/500...'.format(self.counter_Step_2))
                
                if (self.serport.any()):
                    self.prompt = self.serport.read(1)
                    if self.prompt == b's':
                        self.switch_2.write(1)
                        self.transition_to(S19_END_STEP_COLLECT_2)
                    else:
                        pass
                
                if self.counter_Step_2 > 500:
                    self.switch_2.write(1)
                    self.transition_to(S19_END_STEP_COLLECT_2)
                    
                else:
                
                    self._step_time_array_1[self.counter_Step_2] = self.counter_Step_2*.02
                
                    self._step_omega_array_1[self.counter_Step_2] = self.delta_2.read()
                
                    time.sleep(.02)
                
                    self.counter_Step_2 += 1
                    
            elif (self.state==19):
                
                print('Data collection ended.')
                
                for idx in range (0, self.counter_Step_2):
                    print('{:10.2f}, {:10.3f}'.format(self._step_time_array_1[idx],self._step_omega_array_1[idx]))
                
                self.counter_Step_2 = 0
                
                
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)
                
                
                
            elif (self.state==20):
                
                self.motor_1.enable()
                
                self.transition_to(S1_WAIT_FOR_INPUT)
                
            else:
                raise ValueError('Invalid State')

            self.next_time = utime.ticks_add(self.next_time, self.period)
            self.runs += 1

    def transition_to(self, new_state):
        ''' @brief          Transitions the machine from one state to the next
            @details        This function allows for better readability of the code.
                            Rather than prompting the next state with "self.state==1",
                            This function allows the code to be "self.transition_to(__)".
                            State values were initialized with names so that each state's
                            function is indicated in the code as well. Rather than
                            "self.transition_to(1)", the code displays
                            "self.transition_to(S1_WAIT_FOR_INPUT)".
            @param new_state    An integer value indicating the next state
        '''
        self.state = new_state


