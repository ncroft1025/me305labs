'''@file                    main.py
   @brief                   Main function file for the program
   @details                 This file interacts with all other class files in the program:
                            The encoder driver, encoder task class, user task class, and
                            shares class. The main program defines the tasks and runs through
                            them in a continuous loop, while managing the sharing of position
                            and delta information between the classes.
                            
                            The task interaction is handled through common share variables in main.py
                            The interaction is described in the following diagram:
                                
                            \image html task_diagram.PNG "Task Interaction Diagram"
                            
                            
                            The files can be accessed at: https://bitbucket.org/syakel/syakel.bitbucket.io/src/master/
                            
   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    October 12, 2021
'''
# task encoder feeds task user information

import time
import encoder
import task_encoder
import task_user
import shares
import pyb
import DRV8847
import task_controller
import controller

## A task object from the module "encoder.py"
# from the class "Encoder"

def main():
    ''' @brief              Main function for the program
        @details            This function defines the variables of shared information for the program
                            (position and delta), and defines the encoder object, the encoder task,
                            and the user task. The function puts the encoder and user tasks in a list,
                            and runs a continuous loop through these tasks until the program is
                            prompted to terminate via KeyboardInterrupt.
    '''

    # Create an object of the motor driver class
    motor_drv       = DRV8847.DRV8847()
    
    # Create pin objects for the first motor
    B4 = pyb.Pin(pyb.Pin.cpu.B4)
    B5 = pyb.Pin(pyb.Pin.cpu.B5)
    
    # Create pin objects for the second motor
    B0 = pyb.Pin(pyb.Pin.cpu.B0)
    B1 = pyb.Pin(pyb.Pin.cpu.B1)
    
    # Create a motor object for motor 1
    motor_1         = motor_drv.motor(B4, B5, 1, 2)
    
    # Create a motor object for motor 2
    motor_2         = motor_drv.motor(B0, B1, 3, 4)

    # enable the motor driver
    motor_drv.enable()
    
    ## encoder position, shared variable
    enc_pos_1 = shares.Share(0)

    ## delta value, shared variable
    delta_1 = shares.Share(0)
    
    ## encoder 2 position, shared variable
    enc_pos_2 = shares.Share(0)
    
    ## encoder 2 delta, shared variable
    delta_2 = shares.Share(0)

    ## prompt to zero encoder position, shared variable
    flag_1 = shares.Share(0)
    
    flag_2 = shares.Share(0)
    
    # prompt to switch control mode, shared variable
    switch_1 = shares.Share(0)
    
    # prompt to switch control mode, shared variable
    switch_2 = shares.Share(0)
    
    
    ## first pin that tracks encoder 1 position
    pinB6 = pyb.Pin(pyb.Pin.cpu.B6)

    ## second pin that tracks encoder 1 position
    pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
    
    ## first pin that tracks encoder 2 position
    pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
    
    ## second pin that tracks encoder 2 position
    pinC7 = pyb.Pin(pyb.Pin.cpu.C7)

    ## timer that tracks encoder 1 position
    tim4 = pyb.Timer(4, prescaler=0, period= 65535)
    
    ## timer that tracks encoder 2 position
    tim8 = pyb.Timer(8, prescaler=0, period= 65535)
    
    ## Proportional gain, shared variable
    K_p = shares.Share(0)
    
    ## Proportional gain, shared variable
    K_p_2 = shares.Share(0)
    
    ## Velocity set point, shared variable
    omega_ref_1 = shares.Share(0)
    
    ## Velocity set point, shared variable
    omega_ref_2 = shares.Share(0)
    
    ## Velocity measurement, shared variable
    omega_meas_1 = shares.Share(0)
    
    ## Velocity measurment, shared variable
    omega_meas_2 = shares.Share(0)
    
    ## PWM actuation value, shared variable
    PWM_1 = shares.Share(0)
    
    ## PWM actuation value, shared variable
    PWM_2 = shares.Share(0)
    
    ## Loop mode, shared variable
    loop_mode = shares.Share(0)
    
    ## Loop mode, shared variable
    loop_mode_2 = shares.Share(0)
    

    ##defines encoder driver object from the encoder class
    EncoderDriverObj = encoder.Encoder(tim4, pinB6, pinB7)
    
    EncoderDriverObj2 = encoder.Encoder(tim8, pinC6, pinC7)
    
    ## defines controller driver object from the controller class
    ControllerDriverObj = controller.ClosedLoop()
    
    ## defines controller driver object from the controller class
    ControllerDriverObj2 = controller.ClosedLoop()
    
    

    ##period for each update iteration - 2ms
    period = int(0.02)
    
    period_enc = int(0.01)
    

    ## An encoder task object
    task_1 = task_encoder.Task_Encoder(period_enc, delta_1, enc_pos_1, EncoderDriverObj, flag_1)
    
    ## A user interface object
    task_2 = task_user.Task_User(period, delta_1, enc_pos_1, flag_1, delta_2, enc_pos_2, flag_2, loop_mode, loop_mode_2, switch_1, switch_2, K_p, K_p_2, omega_ref_1, omega_ref_2, PWM_1, PWM_2, motor_1, motor_2)
    
    ## An encoder task object
    task_3 = task_encoder.Task_Encoder(period_enc, delta_2, enc_pos_2, EncoderDriverObj2, flag_2)
    
    ## A controller task object
    task_4 = task_controller.Task_Controller(period, K_p, omega_ref_1, omega_meas_1, loop_mode, ControllerDriverObj, switch_1, PWM_1, motor_1, delta_1)
    
    ## A controller task object
    task_5 = task_controller.Task_Controller(period, K_p_2, omega_ref_2, omega_meas_2, loop_mode_2, ControllerDriverObj2, switch_2, PWM_2, motor_2, delta_2)
    
    
    
    ## A list of tasks to run (encoder task, user task)
    task_list = [task_1, task_2, task_3, task_4, task_5]



    while (True):
        try:
            for task in task_list:
                
                task.run()
            
        except KeyboardInterrupt:
            
            break

    print('Program Terminating')


if __name__=='__main__':
    
    main()