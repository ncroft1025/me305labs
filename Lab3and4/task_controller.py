# -*- coding: utf-8 -*-
"""
Created on Thu Oct 28 13:10:38 2021

@author: Seth Yakel
"""

''' @file                   task_controller.py
    @brief
    
    Have a share for gain
    
    do closed loop and motor stuff in same task
    
    @details
    @author                 Seth Yakel
    @author                 Nicole Croft
    @date                   26 October 2021
'''


import controller
import DRV8847
import shares
import utime
from micropython import const
import math


S0_INIT = const(0)

S1_WAIT_FOR_INPUT = const(1)

S2_OPEN_LOOP = const(2)

S3_CLOSED_LOOP = const(3)


class Task_Controller:
    
    def __init__(self, period, K_p, omega_ref, omega_meas, loop_mode, ControllerDriverObj, switch, PWM, motorObject, delta):
        
        self.state = S0_INIT
        
        self.period = period
        
        self.K_p = K_p
        
        self.omega_ref = omega_ref
        
        self.omega_meas = omega_meas
        
        self.loop_mode = loop_mode
        
        self.switch = switch
        
        self.Controller = ControllerDriverObj
        
        self.Motor = motorObject
        
        self.delta = delta
        
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
        self.PWM = PWM
        
        
    def run(self):
        
        if (utime.ticks_us() >= self.next_time):
            
            if (self.state == S0_INIT):
                
                self.transition_to(S1_WAIT_FOR_INPUT)
                
            elif (self.state == S1_WAIT_FOR_INPUT):
                
                self.switch.write(0) 
                
                if (self.loop_mode.read() == 1):
                    
                    self.transition_to(S2_OPEN_LOOP)
                    
                if (self.loop_mode.read() == 2):
                    
                    self.Controller.set_Kp(self.K_p.read())
                    
                    self.transition_to(S3_CLOSED_LOOP)
                    
            elif (self.state == S2_OPEN_LOOP):
                
                self.loop_mode.write(0)
                
                if (self.switch.read() == 1):
                    self.transition_to(S1_WAIT_FOR_INPUT)
                    
                else:
                    self.Motor.set_duty(self.PWM.read())
                    
                    
                    self.transition_to(S1_WAIT_FOR_INPUT)
                
            elif (self.state == S3_CLOSED_LOOP):
                
                self.loop_mode.write(0)
                
                if (self.switch.read() == 1):
                    self.transition_to(S1_WAIT_FOR_INPUT)
                    
                else:
                    #print('Controller updated')
                    
                    omega_meas = float(self.delta.read()*2*math.pi/4000)
                    
                    self.PWM.write(self.Controller.update(float(self.omega_ref.read()), omega_meas))
        
                    self.Motor.set_duty(self.PWM.read())
        
        
    
    def transition_to(self, new_state):
        ''' @brief          Transitions the machine from one state to the next
            @details        This function allows for better readability of the code.
                            Rather than prompting the next state with "self.state==0",
                            This function allows the code to be "self.transition_to(__)".
                            State values were initialized with names so that each state's
                            function is indicated in the code as well. Rather than
                            "self.transition_to(0)", the code displays
                            "self.transition_to(S0_INIT)".
        '''
        self.state = new_state
    
    
    
    
    
    
    
    
    
    
    