'''@file                    task_encoder.py
   @brief                   Constructs the finite state machine for task_encoder
   @details                 task_encoder includes three functions: one to initialize appropriate
                            variables, one to carry out appropriate actions within each state
                            and the proper movement between them, and one to
                            carry out the transition between each of the states. The final function
                            is not necessary, but makes the code more readable and user-friendly.
                            See description of the self.transition_to function for more details.
                            
                            The task_encoder class diagram has two states and performs as follows:
                                
                            \image html task_encoder_states.PNG "Task Encoder State Diagram"
                            
   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    October 18, 2021
'''

import utime
import encoder
import math

## @brief A variable representing state 0
#  @details This state is the initialization state
#
S0_INIT         = 0

## @brief A variable representing state 1
#  @details This state writes data to the shared variables enc_pos and delta
#
S1_TRACK_POS    = 1

#Define a class for the encoder's finite state machine
class Task_Encoder:
    ''' @brief              Constructs an encoder task class
        @details            The class contains three functions: init, run, and transition_to.
                            The class defines appropriate variables and maps the prompts and
                            actions required by the task_user finite state machine.
    '''

    # Define the constructor
    def __init__(self, period, delta, enc_pos, EncoderDriverObj, flag):
        ''' @brief          Constructs an encoder object
            @details        The init function initializes the state machine to the S0 INIT state,
                            initializes runs of the state machine to zero, and defines the serial
                            port so that the file can read user imports from the keyboard to
                            transition between states.
        '''
        ## The state to run in the finite state machine
        self.state = S0_INIT

        ## Number of runs of the finite state machine
        self.runs = 0

        ## The period of the task (in ms)
        self.period = period 

        ## A shares.Share object representing encoder position
        self.enc_pos = enc_pos

        ## A shares.Share object representing change in encoder position
        self.delta = delta

        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
        ## @brief An object equal to the EncoderDriverObj which is passed into the class
        #  @details This is set equal to the object passed in
        #
        
        self.encoder1 = EncoderDriverObj
        
        ## @brief A shared trigger variable used in between task_encoder and task_user
        #  @details Used to zero the position of the encoder by switching from low to high
        #
        
        self.flag = flag


    def run(self):
        ''' @brief          Constructs the flow of the task_encoder finite state machine
            @details        The task_encoder finite state machine has seven states: S0-S6.
                            This function runs each of these states and defines the proper
                            transitions between them according to the state diagram.
                            The serial import variable defined in the init function is utilized
                            so that user inputs can be interpreted and prompt the function to
                            transition between the states.
        '''
        if (utime.ticks_us() >= self.next_time):
            if (self.state == S0_INIT):
                self.transition_to(S1_TRACK_POS)
            elif (self.state == S1_TRACK_POS):
                
                if (self.flag.read() == 1):
                    
                    
                    self.encoder1.set_position()

                    
                    self.enc_pos.write(self.encoder1.get_position())
                    
                    self.delta.write(self.encoder1.get_delta())
                    
                    self.flag.write(0)
                    
                    self.next_time += self.period
                    
                    self.transition_to(S0_INIT)
                    
                elif (self.flag.read() == 0):
                    
                    self.encoder1.update()
                
                    self.enc_pos.write(self.encoder1.get_position())
                    self.delta.write(self.encoder1.get_delta())
                
                    self.next_time += self.period
                
                    self.transition_to(S0_INIT)
                

    def transition_to(self, new_state):
        ''' @brief          Transitions the machine from one state to the next
            @details        This function allows for better readability of the code.
                            Rather than prompting the next state with "self.state==0",
                            This function allows the code to be "self.transition_to(__)".
                            State values were initialized with names so that each state's
                            function is indicated in the code as well. Rather than
                            "self.transition_to(0)", the code displays
                            "self.transition_to(S0_INIT)".
        '''
        self.state = new_state
