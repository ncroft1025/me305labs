''' @file                   BNO055.py
    @brief                  Constructs a driver class for the BNO055 IMU Sensor
    @details                Used in the main function file to create objects of the IMU sensor to
                            measure orientation data.
                            
    @author                 Seth Yakel
    @author                 Nicole Croft
    @date                   December 9, 2021
'''

import pyb

from pyb import I2C

import struct

class BNO055:
    ''' @brief      IMU driver class
        @details    Objects of this class can be used to configure the BNO055
                    IMU driver and measure the orientation data and calibrate the sensor.
    '''
    
    def __init__(self, _I2C_obj):
        ''' @brief              Initializes and returns an IMU driver object
            @details            Takes in required parameters from the main function file and
                                initializes all other required variables to perform all the methods
                                in the class.
            @param _I2C_obj     I2C object
        '''
        
        self.I2C = _I2C_obj
        
        self.change_opMode(0)
        
        self.change_opMode(12)
        
        pass
    
    def change_opMode(self, value):
        ''' @brief          Changes the operation mode
            @param value    Operation mode to change to
        '''
        
        self.I2C.mem_write(value, 0x28, 0x3D)
        
        pass
    
    def get_calibrationStatus(self):
        ''' @brief          Gets the calibration status 
            @return         Calibration status 
        '''
        
        cal_bytes = bytearray(1)
        
        cal_bytes = self.I2C.mem_read(cal_bytes, 0x28, 0x35)
        
        print("Binary:", '{:#010b}'.format(cal_bytes[0]))

        cal_status = ( cal_bytes[0] & 0b11,
                     (cal_bytes[0] & 0b11 << 2) >> 2,
                     (cal_bytes[0] & 0b11 << 4) >> 4,
                     (cal_bytes[0] & 0b11 << 6) >> 6)

        print("Values:", cal_status)
        print('\n')
        
        return cal_bytes[0]
    
    def get_calibrationCoeff(self):
        ''' @brief          Gets the calibration coefficients
            @return         Calibration status as a tuple
        '''
        
        calib = bytearray(22)
        
        self.I2C.mem_read(calib, 0x28, 0x55)
        
        calib_signed_ints = struct.unpack('<hhhhhhhhhhh', calib)
        
        calib_value = tuple(calib_int/16 for calib_int in calib_signed_ints)
        
        
        return calib_value
    
    def write_calibrationCoeff(self, cal_buf):
        ''' @brief          Writes calibration coefficients to the sensor
            @param cal_buf  Buffer containing the coefficients to be written.
        '''
        
        self.I2C.mem_write(cal_buf, 0x28, 0x55)
        
        pass
    
    def read_eulerAngles(self):
        ''' @brief          Reads the euler angles
            @return         Euler angles in a tuple
        '''
        
        buf = bytearray(6)
        
        self.I2C.mem_read(buf, 0x28, 0x1A)
        
        eul_signed_ints = struct.unpack('<hhh', buf)
        
        eul_val = tuple(eul_int/16 for eul_int in eul_signed_ints)
        
        
        return eul_val
    
    def read_angularVelocity(self):
        ''' @brief          Reads the angular velocity
            @return         Angular velocities in a tuple
        '''
        
        buf = bytearray(6)
        
        self.I2C.mem_read(buf, 0x28, 0x14)
        
        omega_signed_ints = struct.unpack('<hhh', buf)
        
        omega_val = tuple(omega_int/16 for omega_int in omega_signed_ints)
        
        
        return omega_val
    
    