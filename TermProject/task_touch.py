'''@file                    task_touch.py
   @brief                   Constructs the finite state machine for the controller task
   @details                 Task for interaction with the touch panel.
                            A FSM which continuously updates the variables
                            containing the ball position information.
                            
                            See below for the state diagram.
                        
                            \html ???
   
   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    December 9, 2021
'''

import utime
import touch_panel
import math
from micropython import const
import os

## @brief   File for which to write the calibration coefficients
#
filename = "RT_cal_coeffs.txt"

## @brief   State 0, initialization
#
S0_INIT = const(0) 

## @brief   State 1, updating
#
S1_UPDATE = const(1)



class Task_Touch:
    ''' @brief      Touch panel task class
        @details    An object of this class is created in the main function file to
                    perform necessary tasks as outlined in the finite state machine.
    '''
    
    
    def __init__(self, period, x, x_dot, y, y_dot, TouchPanelObj):
        ''' @brief                  Initializes and returns a touch panel task object
            @details                Takes in required parameters from the main function file and
                                    initializes all other required variables to run the state machine.
                                    This includes creating a touch panel driver object and setting
                                    initial KT values outlined in the assignment, and setting the
                                    state of the FSM initially to S0_INIT.
            @param period           Period for the touch panel task (2 ms)
            @param x                position of the ball (x)
            @param x_dot            velocity of the ball (x)
            @param y                position of the ball (y)
            @param y_dot            velocity of the ball (y)
            @param TouchPanelObj    Touch panel driver object
        '''
        
        self._period = int(period)
        
        self._x = x
        
        self._x_dot = x_dot
        
        self._y = y
        
        self._y_dot = y_dot
        
        self.next_time = utime.ticks_add(utime.ticks_us(), self._period)
        
        self._panel = TouchPanelObj
        
        self._last_x = self._panel.x_scan()
        
        self._next_x = self._panel.x_scan()
        
        self._last_y = self._panel.y_scan()
        
        self._next_y = self._panel.y_scan()
        
        self._last_time = utime.ticks_us()
        
        self._next_time = utime.ticks_us()
        
        self._v_k_x = 0
        
        self._v_k_y = 0
        
        self.state = S0_INIT
        
    def run(self):
        ''' @brief      Runs the touch panel task
            @details    Runs the FSM to move from a calibration state
                        to a continuous update state. The position and velocity
                        of the ball are calculated using an alpha beta filter
                        in the update state.
        '''
        
        if (utime.ticks_us() >= self.next_time):
            
            
            if (self.state == S0_INIT):
                
                if filename in os.listdir():
                    
                    with open(filename, 'r') as f:
                        
                        # Read the first line of the file
                        
                        cal_data_string = f.readline()
                        
                        # Split the line into multiple strings
                        # and then convert each one to a float
                        
                        cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
                        
                        self.Kxx = cal_values[0]
                        
                        self.Kxy = cal_values[1]

                        self.Kyx = cal_values[2]
                        
                        self.Kyy = cal_values[3]
                        
                        self.Xc  = cal_values[4]
                        
                        self.Yc  = cal_values[5]
                        
                        self.transition_to(S1_UPDATE)
                        
                else:
                
                    with open(filename, 'w') as f:
                    # Perform manual calibration
                    
                        (Kxx, Kxy, Kyx, Kyy, Xc, Yc) = self._panel.calibrate()
                    
                        # Then, write the calibration coefficients to the file
                        # as a string. The example uses an f-string, but you can
                        # use string.format() if you prefer
                    
                        #f.write(str(Kxx), str(Kxy), str(Kyx), str(Kyy), str(Xc), str(Yc))
                
                        f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n")
                
                
            
            elif(self.state == S1_UPDATE):
                
                ## This state is continuously run in order to
                ## update the shared ball position variables
                ## using methods from the touch_panel driver
                ## and an alpha-beta filter.
                
                
                if (self._panel.z_scan() == 0):
                    
                    self._x.write(0)
                    
                    #print('X Position: ' + '0')
                    
                    self._y.write(0)
                    
                    #print('Y Position: ' + '0')
                    
                    
                else:
                
                    self._next_time = utime.ticks_us()
                    
                    T_s = utime.ticks_diff(self._next_time, self._last_time)
                    
                    # Take position measurements from the touch panel driver
                    
                    x_m = self._panel.x_scan()
                    
                    y_m = self._panel.y_scan()
                    
                    # Apply the touch panel calibration equation
                    
                    x = x_m*self.Kxx + y_m*self.Kxy + self.Xc
                    
                    y = x_m*self.Kyx + y_m*self.Kyy + self.Yc
                    
                    # Apply the alpha beta filter
                    
                    x_k_next = self._last_x + 0.85*(x - self._last_x) + T_s*self._v_k_x
                    
                    v_k_x_next = self._v_k_x + (0.005/T_s)*(x - x_k_next)
                    
                    y_k_next = self._last_y + 0.85*(y - self._last_y) + T_s*self._v_k_y
                    
                    v_k_y_next = self._v_k_y + (0.005/T_s)*(x - y_k_next)
                    
                    ## Write to the shared position and velocity variables
                    
                    self._x.write(x_k_next)
                    
                    #print('X Position: ' + str(x))
                    
                    self._y.write(y_k_next)
                    
                    #print('Y Position: ' + str(y))
                    
                    self._x_dot.write(v_k_x_next)
                    
                    self._y_dot.write(v_k_y_next)
                    
                    ## Reset all values for the next iteration
                    
                    x_k = x_k_next
                    
                    y_k = y_k_next
                    
                    self._v_k_x = v_k_x_next
                    
                    self._v_k_y = v_k_y_next
                    
                    #utime.sleep(2)
                    
                    self.next_time += self._period

                
                
                
    def transition_to(self, new_state):
        ''' @brief          Transitions the machine from one state to the next
            @details        This function allows for better readability of the code.
                            Rather than prompting the next state with "self.state==0",
                            This function allows the code to be "self.transition_to(__)".
                            State values were initialized with names so that each state's
                            function is indicated in the code as well. Rather than
                            "self.transition_to(0)", the code displays
                            "self.transition_to(S0_INIT)".
        '''
        self.state = new_state
        