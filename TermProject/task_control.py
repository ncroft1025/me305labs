'''@file                    task_control.py
   @brief                   Constructs the finite state machine for the controller task
   @details                 Task for interaction with the motor controls.
                            A FSM which continuously updates the PWM for both
                            motors using the controller and motor drivers.

                            See below for the controller task finite state diagram:
                            \image html ???
   
   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    December 9, 2021
'''
import utime
import controller_fsfb

## @brief   State 0, initialization
#
S0_INIT = 0

## @brief   State 1, updating
#
S1_UPDATE = 1

class Task_Control:
    ''' @brief          User task class
        @details        An object of this class is created in the main function file to
                        perform necessary tasks as outlined in the finite state machine.
    '''
    
    def __init__(self, period, x, x_dot, y, y_dot, th_x, th_x_d, th_y, th_y_d, motorObj_1, motorObj_2, balanceCMD):
        ''' @brief                  Initializes and returns a controller task object
            @details                Takes in required parameters from the main function file and
                                    initializes all other required variables to run the state machine.
                                    This includes creating controller driver class objects, setting
                                    initial KT values outlined in the assignment, and setting the
                                    state of the FSM initially to S0_INIT.
            @param period           Period for the controller task (1 ms)
            @param x                ball position (x)
            @param x_dot            ball velocity (x)
            @param y                ball position (y)
            @param y_dot            ball velocity (y)
            @param th_x             platform angle (x)
            @param th_x_d           platform angular velocity (x)
            @param th_y             platform angle (y)
            @param th_y_d           platform angular velocity (y)
            @param motorObj_1       Motor object 1
            @param motorObj_2       Motor object 2
            @param balanceCMD       Shared object representing the user's command to begin balancing the ball

        '''
        
        self._period = int(period)
        
        self._balanceCMD = balanceCMD
        
        self._x = x
        
        self._x_dot = x_dot
        
        self._y = y
        
        self._y_dot = y_dot
        
        self._th_y = th_y
        
        self._th_y_d = th_y_d
        
        self._th_x = th_x
        
        self._th_x_d = th_x_d
        
        self._motor1 = motorObj_1
        
        self._motor2 = motorObj_2
        
        self.next_time = utime.ticks_add(utime.ticks_us(), self._period)
        
        self.state = 0

        ## @brief   Controller driver object 1 (for first set of KT initial conditions)
        #
        self.Control_1 = controller_fsfb.ClosedLoop()

                                #x       th_y       x_dot     th_y_dot
        self.Control_1.set_KT(-0.00038, 0.38774, -0.0040, 0.014574)

        ## @brief   Controller driver object 2 (for second set of KT initial conditions)
        self.Control_2 = controller_fsfb.ClosedLoop()
        
                                #y       th_x       y_dot     th_x_dot
        self.Control_2.set_KT(-0.00038, -0.37774, -0.0040, -0.014574)
        
        
    def run(self): 
        '''@brief       Runs the control task.
           @details     Uses a finite state machine to begin platform control
                        depending on commands from the user.
        '''
        
        if (utime.ticks_us() >= self.next_time):
            
            
            if (self.state == S0_INIT):
                
                if self._balanceCMD.read() == True:
                    
                    self.transition_to(S1_UPDATE)
                
            
            elif(self.state == S1_UPDATE):
                
                # Duty cycle sent to T_x motor
                D_1 = self.Control_1.update(self._x.read(),self._th_y.read(), self._x_dot.read(), self._th_y_d.read())
                
                # Duty cycle sent to T_y motor
                D_2 = self.Control_2.update(self._y.read(), self._th_x.read(), self._y_dot.read(), self._th_x_d.read())
                
               # if self._th_x.read() > -10:
                    
                    #D_2 = D_2*1.2
                    
                # if self._th_x.read() > -13:
                #     D_2 = D_2*1.3
                
                if self._x.read() > 0:
                    D_1 = -D_1
                    
                if self._y.read() > 0:
                    D_2 = -D_1
                    
                if self._x.read() < 0:
                    D_1 = -D_1
                    
                if self._y.read() < 0:
                    D_2 = -D_1
                
                self._motor1.set_duty(D_1)
                #print('Duty M_y')
                #print(D_1)
                
                self._motor2.set_duty(D_2)
                #print('Duty M_x')
                #print(D_2)
                
                
                if self._balanceCMD.read() == False:
                    
                    self.transition_to(S0_INIT)
                
                
                
                
                
                
                
    def transition_to(self, new_state):
        ''' @brief          Transitions the machine from one state to the next
            @details        This function allows for better readability of the code.
                            Rather than prompting the next state with "self.state==0",
                            This function allows the code to be "self.transition_to(__)".
                            State values were initialized with names so that each state's
                            function is indicated in the code as well. Rather than
                            "self.transition_to(0)", the code displays
                            "self.transition_to(S0_INIT)".
        '''
        self.state = new_state