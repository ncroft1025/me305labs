'''@file                    touch_panel.py
   @brief                   Constructs a driver class for the resistive touch panel
   @details                 A class from which touch panel objects can be 
                            created to measure the balls position. This
                            file has been optimised for speed. The calibration method
                            utilizes a finite state machine to walk the user through
                            touch panel calibration. See calibrate() for
                            the finite state diagram and more details.
   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    December 9, 2021
'''


import pyb
import utime
from ulab import numpy as np
from micropython import const


class Touch_Panel :
    ''' @brief
        @details
    '''
    
    def __init__(self, x_p, x_m, y_p, y_m, Y_dim, X_dim, center_y, center_x):
        ''' @brief              Completes setup and initializes the appropriate parameters
            @details            The device is a four-wire resistive touch panel. Position
                                is measured according to the voltage drop between the positive
                                and negative pins in a given direction. The positive and negative
                                pins are initialized here.
            @param x_p          x pin (positive)
            @param x_m          x pin (negative)
            @param y_p          y pin (positive)
            @param y_m          y pin (negative)
            @param Y_dim
            @param X_dim
            @param center_y     center, y direction
            @param center_x     center, x direction
            '''
        
        self._x_p = pyb.Pin(x_p)
        
        self._x_m = pyb.Pin(x_m)
        
        self._y_p = pyb.Pin(y_p)
        
        self._y_m = pyb.Pin(y_m)
        
        self._Ydim = Y_dim
        
        self._Xdim = X_dim
        
        self._Ycenter = center_y
        
        self._Xcenter = center_x
        
        
        
    def x_scan(self):
        '''@brief       Returns the digital x reading.
           @details     
           @return      Output from x reading
        '''
        
        self._x_p.init(self._x_p.OUT_PP, value = 1)
        
        self._x_m.init(self._x_m.OUT_PP, value = 0)
        
        self._y_m.init(self._y_m.IN)
        
        self._y_p.init(self._y_p.IN)
        
        output = pyb.ADC(self._y_m)
        
        return output.read()
    
    def y_scan(self):
        '''@brief       Returns the digital y reading.
           @details     
           @return      Output from y reading
        '''
        
        self._y_p.init(self._y_p.OUT_PP, value = 1)
        
        self._y_m.init(self._y_m.OUT_PP, value = 0)
        
        self._x_m.init(self._x_m.IN)
        
        self._x_p.init(self._x_p.IN)
        
        output = pyb.ADC(self._x_m)
        
        return output.read()
    
    def z_scan(self):
        '''@brief       Returns the digital z reading.
           @details     
           @return      Output from z reading
        '''
        
        
        self._y_p.init(self._y_p.OUT_PP, value = 1)
        
        self._x_m.init(self._x_m.OUT_PP, value = 0)
        
        self._y_m.init(self._y_m.IN)
        
        self._x_p.init(self._x_p.IN)
        
        output = pyb.ADC(self._y_m)
        
        if output.read() < 4000:
            
            y_m = 1
            
        else:
            
            y_m = 0
        
        return y_m
    
    def calibrate(self):
        '''@brief       Prompts the user through the calibration procedure.
           @details     Uses a finite state machine to walk the user through
                        calibration of the touch panel.
                        See calibration equations and the state diagram below.
                        \image html ???
        '''
        
        # S0_ORIGIN = 1
        # S1_ONE = 2
        # S2_TWO = 3
        # S3_THREE = 4
        # S4_FOUR = 5
        # S5_FIVE = 6
        # S6_SIX = 7
        # S7_CALC = 8
        
        state = 1
        
        print('Please touch the panel at (0, 0)')
        
        while (state == 1):
            
            if self.z_scan() == 1:
                
                ADC_x1 = self.x_scan()
                ADC_y1 = self.y_scan()
                
                state = 2
                
                print('Input registered')
                
                utime.sleep(1)
                
                print('Please touch the panel at (40, 0)')
                
            
        while (state == 2):
            
            if self.z_scan() == 1:
                
                ADC_x2 = self.x_scan()
                ADC_y2 = self.y_scan()
                
                state = 3
                
                print('Input registered')
                
                utime.sleep(1)
                
                print('Please touch the panel at (-40, 0)')
                
        
        while (state == 3):
            
            if self.z_scan() == 1:
                
                ADC_x3 = self.x_scan()
                ADC_y3 = self.y_scan()
                
                state = 4
                
                print('Input registered')
                
                utime.sleep(1)
                
                print('Please touch the panel at (0, 40)')
                
            
        while (state == 4):
            
            if self.z_scan() == 1:
                
                ADC_x4 = self.x_scan()
                ADC_y4 = self.y_scan()
                
                state = 5
                
                print('Input registered')
                
                utime.sleep(1)
                
                print('Please touch the panel at (0, -40)')
                
            
        while (state == 5):
            
            if self.z_scan() == 1:
                
                ADC_x5 = self.x_scan()
                ADC_y5 = self.y_scan()
                
                state = 6
                
                print('Input registered')
                
                utime.sleep(1)
                
                print('Please touch the panel at (40, 40)')
                
            
        while (state == 6):
            
            if self.z_scan() == 1:
                
                ADC_x6 = self.x_scan()
                ADC_y6 = self.y_scan()
                
                state = 7
                
                print('Input registered')
                
                utime.sleep(1)
                
                print('Please touch the panel at (80, -40)')
                
            
        while (state == 7):
            
            if self.z_scan() == 1:
                
                ADC_x7 = self.x_scan()
                ADC_y7 = self.y_scan()
                
                state = 8
                
                print('Input registered')
                
                utime.sleep(1)
                
            
        if (state == 8):
            
            X = np.array([[ADC_x1, ADC_y1, 1], [ADC_x2, ADC_y2, 1],
                         [ADC_x3, ADC_y3, 1], [ADC_x4, ADC_y4, 1],
                         [ADC_x5, ADC_y5, 1], [ADC_x6, ADC_y6, 1],
                         [ADC_x7, ADC_y7, 1]])
            
            x = np.array([[0, 0], [40, 0], [-40, 0], [0, 40], [0, -40], [40, 40], [80, -40]])
            
            t = np.dot(X.transpose(), X)
            
            T1 = np.linalg.inv(t)
            
            T2 = np.dot(X.transpose(), x)
            
            self.B = np.dot(T1, T2)
            
            print(self.B)
            
            return (self.B[0, 0], self.B[1, 0], self.B[0, 1], self.B[1, 1], self.B[2, 0], self.B[2, 1])
            
    
    
    def all_scan(self):
        '''@brief       Returns the digital x, y, and z readings in a tuple.
           @return      A tuple containing x, y, and z
        '''
        
        # X Scan
        
        self._x_p.init(self._x_p.OUT_PP, value = 1)
        
        self._x_m.init(self._x_m.OUT_PP, value = 0)
        
        self._y_m.init(self._y_m.IN)
        
        self._y_p.init(self._y_p.IN)
        
        output_x = pyb.ADC(self._y_m)
        
        x = output_x.read()
        
        
        
        # Z Scan
        
        self._x_p.init(self._x_p.IN)
        
        self._y_p.init(self._y_p.OUT_PP, value = 1)
        
        self._y_m.init(self._y_m.IN)
        
        output_z = pyb.ADC(self._y_m)
        
        if output_z.read() < 4000:
            
            z = 1
            
        else:
            
            z = 0
        
        
        
        # Y Scan
        
        self._y_m.init(self._y_m.OUT_PP, value = 0)
        
        self._x_m.init(self._x_m.IN)
        
        output_y = pyb.ADC(self._x_m)
        
        y = output_y.read()
        
        
        tup = (x, y, z)
        
        return tup
    
    
    
    
    

    
    
    
        
        