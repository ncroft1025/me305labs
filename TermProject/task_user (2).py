'''@file                    task_user.py
   @brief                   Constructs the finite state machine for the user task
   @details                 The finite state machine allows the user to command the system
                            to begin and terminate platform balancing, and
                            begin and terminate data collection of ball and platform positions
                            and velocities.
                            
                            The user task has 4 distinct states according to the state diagram below.
                            
                            \image html ???
                            
                           

   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    December 9, 2021
'''

import pyb, utime
import time
import array as arr
import math
from micropython import const

## @brief   State 0, initialization
#
S0_INIT = const(0)

## @brief   State 1, waits for user input and responds to commands accordingly
#
S1_WAIT_FOR_INPUT = const(1)


class Task_User:
    ''' @brief              User task class
        @details            An object of this class is created in the main function file to
                            perform necessary tasks as outlined in the finite state machine.
    '''
    
    def __init__(self, period, balanceCMD, dataCMD):
        ''' @brief              Initializes and returns a user task object
            @details            Takes in required parameters from the main function file and
                                initializes all other required variables to run the state machine.
                                This includes initializing the serial port to take in user commands
                                from the keyboard, and setting the state to S0_INIT.
            @param period       Period for the user task (2 ms)
            @param balanceCMD   Shared object representing the user's command to begin balancing the ball
            @param dataCMD      Shared object representing the user's command to begin collecting data
        '''
        
        self.period = int(period)
        
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
        self.state = 0

        ## @brief   Serial port to take commands from the user
        #
        self._serport = pyb.USB_VCP()
        
        self._balanceCMD = balanceCMD

        self._dataCMD = dataCMD
        
        self._command = S0_INIT
        
        self.state = S0_INIT
        
        pass
    
    
    def run(self):
        '''@brief       Runs the user task.
           @details     Uses a finite state machine to interact with the user.
                        Commands are entered through characters at the serial
                        port. See list_commands() for command options.
                        
        '''
        
        current_time = utime.ticks_us()
        
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            
            if self.state == 0:
                
                print("The available commands are:")
                print("    'b' or 'B': balance ball")
                print("    's' or 'S': stop balancing ball")
                print("    'c' or 'C': begin data collection")
                print("    't' or 'T': terminate data collection")
            
                self.transition_to(1)
            
            elif self.state == 1:
                
            
                if self._serport.any():
                    self._command = self._serport.read(1)
        
                    if self._command == b'b' or self._command == b'B':
                        self._balanceCMD.write(True)
                        print('Balancing Ball ...')
                        self._command = 0
                        self.transition_to(0)
                    
                    elif self._command == b's' or self._command == b'S':
                        self._balanceCMD.write(False)
                        print('Stoping Balancing Ball')
                        self._command = 0
                        self.transition_to(0)

                    elif self._command == b'c' or self._command == b'C':
                        self._dataCMD.write(True)
                        print('Collecting Data ...')
                        self._command = 0
                        self.transition_to(0)

                    elif self._command == b't' or self._command == b'T':
                        self._dataCMD.write(False)
                        print('Data Collection Terminated')
                        self.transition_to(0)
                    
            
            self.next_time = current_time
                
                
                
    def transition_to(self, new_state):
        ''' @brief          Transitions the machine from one state to the next
            @details        This function allows for better readability of the code.
                            Rather than prompting the next state with "self.state==0",
                            This function allows the code to be "self.transition_to(__)".
                            State values were initialized with names so that each state's
                            function is indicated in the code as well. Rather than
                            "self.transition_to(0)", the code displays
                            "self.transition_to(S0_INIT)".
        '''
        self.state = new_state
        