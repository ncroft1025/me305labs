''' @file                   controller_fsfb.py
    @brief                  Constructs a driver class for the full state feedback controller
    
    @details                ClosedLoop includes 3 functions: one to initialize the appropriate
                            variables, one to calculate the error and produce an acuation value,
                            and one to set the proportional gain KT according to user input.
                            
                            The calculation of the actuation value is detailed in the 
                            document below:
                                
                            \image ???
                            
    @author                 Seth Yakel
    @author                 Nicole Croft
    @date                   December 9, 2021
'''
from ulab import numpy as np

class ClosedLoop:
    
    ''' @brief              Defines the Closed Loop Class
        @details            Objects of this class can be used to perform closed
                            loop control. This class was written so that it 
                            can be used as a general controller.
    '''
    
    def __init__(self):
        ''' @brief          Completes setup and initializes the appropriate
                            parameters
            @details        No additional parameters or initialization is necessary
                            for the controller driver
        '''
        
        
        pass
    
    def update(self, ssv_1_meas, ssv_2_meas, ssv_3_meas, ssv_4_meas):
        ''' @brief              Computes and returns the actuation value based on
                                the measured and reference values
            @details
            
            @param ssv_1_meas   State Space Variable 1
            @param ssv_2_meas   State Space Variable 2
            @param ssv_3_meas   State Space Variable 3
            @param ssv_4_meas   State Space Variable 4
            
            @return             Actuation value
        '''
        pos = 0 - ssv_1_meas
        
        theta = 0 - ssv_2_meas
        
        vel = 0 - ssv_3_meas
        
        theta_dot = 0 - ssv_4_meas
        
        x = np.array([[pos], [theta], [vel], [theta_dot]])
        
        T_x = -np.dot(self.K_T, x) 
        
        
        actuation = (100*(0.25)*T_x[0]*(2.21))/(12*(0.0138))
        
        
        return actuation
        
        pass
    
    def set_KT(self, K_1, K_2, K_3, K_4):
        ''' @brief          Sets the calculated Kp value
            @param K_1      Gain K_1 value
            @param K_2      Gain K_2 value
            @param K_3      Gain K_3 value
            @param K_4      Gain K_4 value
        '''
        self.K_T = np.array([K_1, K_2, K_3, K_4])
        
        pass