'''@file                    task_IMU.py
   @brief                   Constructs the finite state machine for the IMU task
   @details                 Creates a task for interaction with the IMU device.
                            A FSM which continuously updates the variables
                            containing the platform euler angle information.
                            
                            See state diagram below.
                            
                            \image ???
   
    
   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    December 9, 2021
'''

import utime
import BNO055
import math
import os

## @brief   State 0, initialization
#
S0_INIT = 0

## @brief   State 1, updating
#
S1_UPDATE = 1

## @brief   File for which to write calibration coefficients
#
filename = 'IMU_cal_coeffs.txt'

class Task_IMU:
    ''' @brief          IMU task class
        @details        An object of this class is created in the main function file to
                        perform necessary tasks as outlined in the finite state machine.
    '''
    def __init__(self, period, th_x, th_x_d, th_y, th_y_d, IMUObj):
        ''' @brief              Initializes and returns an IMU task object
            @details            Takes in required parameters from the main function file and
                                initializes all other required variables to run the state machine.
                                This includes setting the state of the FSM initially to S0_INIT.
            @param period       Period of the IMU task (2 ms)
            @param th_x         platform angle (x)
            @param th_x_d       platform angular velocity (x)
            @param th_y         platform angle (y)
            @param th_y_d       platform angular velocity (y)
            @param IMUObj       IMU Object
        '''
        
        self._period = int(period)
        
        self._th_y = th_y
        
        self._th_y_d = th_y_d
        
        self._th_y_dd = th_y_d
        
        self._th_x = th_x
        
        self._th_x_d = th_x_d
        
        self._th_x_dd = th_x_d
        
        self.imu = IMUObj
        
        self.next_time = utime.ticks_add(utime.ticks_us(), self._period)
        
        self.state = 0
        
        if filename in os.listdir():
            self.autocalibrate()
        
    def autocalibrate(self):
        '''@brief       Calibrates the IMU
           @details     Uses a file supplied by the user to write calibration 
                        coefficients to the IMU.
                        
        '''
        
        with open(filename, 'r') as f:
            
            cal_data_string = f.readline()
            
            
            cal_buf = bytes(int(cal_value) for cal_value in cal_data_string.strip().split(','))
            self.imu.write_calibrationCoeff(cal_buf)
            
            #print('IMU bytes:', cal_buf)
            #print('IMU Calibration Complete.')
            
        
    def run(self): 
        '''@brief       Runs the IMU task.
           @details     Runs the finite state machine to continuously update the
                        orientation data using methods from the BNO055 object.
        '''
        
        if (utime.ticks_us() >= self.next_time):
            
            
            if (self.state == S0_INIT):
                self.transition_to(S1_UPDATE)
                
            
            elif(self.state == S1_UPDATE):
                
                eul_angles = self.imu.read_eulerAngles()
                
                #print(eul_angles)
                
                omega = self.imu.read_angularVelocity()
                
                #print(omega)
                
                self._th_y.write((eul_angles[2]*math.pi)/180)
                
                self._th_x.write((eul_angles[1]*math.pi)/180)
                
                self._th_y_d.write((omega[2]*math.pi)/180)
                
                self._th_x_d.write((omega[1]*math.pi)/180)
                
                self.transition_to(S0_INIT)
                
                
                
    def transition_to(self, new_state):
        ''' @brief          Transitions the machine from one state to the next
            @details        This function allows for better readability of the code.
                            Rather than prompting the next state with "self.state==0",
                            This function allows the code to be "self.transition_to(__)".
                            State values were initialized with names so that each state's
                            function is indicated in the code as well. Rather than
                            "self.transition_to(0)", the code displays
                            "self.transition_to(S0_INIT)".
        '''
        self.state = new_state
        
                
                
                
                
                
                
                