'''@file                    main.py
   @brief                   Main function file for the program
   @details                 Instantiates the objects and tasks using imported
                            modules and performs multithreading of each
                            necessary task.
    
   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    December 9, 2021
'''

# Import necessary modules
import utime
import shares
import pyb
from pyb import I2C

# Import necessary task files
import task_touch
import task_IMU
import task_control
import task_user

# Import necessary driver files
import touch_panel
import BNO055
import motor_driver




def main():
    ''' @brief              Main function for the program
        @details            Creates all necessary share objects for collaborative multitasking
                            (ball position and velocity, platform angle and angular velocity,
                            command variables from the user). Creates pin objects for the touch
                            panel and motor drivers, and all other necessary objects (motor 1, motor 2,
                            motor driver, I2C, touch panel driver, IMU driver).
                            Finally, each of the tasks from our high-level task diagram is created
                            (user, controller, touch panel, IMU, and data collection) with appropriate
                            frequencies. After all required instantiation is complete, the program
                            runs each of these tasks in a continuous loop that can be stopped with a
                            Keyboard Interrupt.
    '''
    
    # Create share objects for ball position and velocity

    ## @brief   ball position (x)
    #
    x = shares.Share(0)

    ## @brief   ball velocity (x)
    #
    x_dot = shares.Share(0)

    ## @brief   ball position (y)
    #
    y = shares.Share(0)

    ## @brief   ball velocity (y)
    #
    y_dot = shares.Share(0)
    
    # Create share objects for platform angle and angular velocity

    ## @brief   platform angle (x)
    #
    theta_x = shares.Share(0)

    ## @brief   platform angular velocity (x)
    #
    theta_x_dot = shares.Share(0)

    ## @brief   platform angle (y)
    #
    theta_y = shares.Share(0)

    ## @brief   platform angular velocity (y)
    theta_y_dot = shares.Share(0)
    
    # Create share objects for command variables

    ## @brief   command to balance the ball
    #
    balanceCMD = shares.Share(0)

    ## @brief   command to collect data
    #
    dataCMD = shares.Share(0)
    
    # Create pins for touch panel driver
    xm = pyb.Pin.cpu.A7
    xp = pyb.Pin.cpu.A1
    ym = pyb.Pin.cpu.A6
    yp = pyb.Pin.cpu.A0
    
    # Create pin objects for the first motor (T_y)
    B4 = pyb.Pin(pyb.Pin.cpu.B4)
    B5 = pyb.Pin(pyb.Pin.cpu.B5)
    
    # Create pin objects for the second motor (T_x)
    B0 = pyb.Pin(pyb.Pin.cpu.B0)
    B1 = pyb.Pin(pyb.Pin.cpu.B1)
    
    ## @brief   Motor driver object
    #
    motor_drv       = motor_driver.Motor_Driver()
    
    ## @brief   Motor object for motor 1 (T_y)
    #
    motor_1         = motor_drv.motor(B4, B5, 1, 2)
    
    ## @brief   Motor object for motor 2 (T_x)
    #
    motor_2         = motor_drv.motor(B0, B1, 3, 4)
    
    ## @brief   I2C object
    #
    i2c = I2C(1, I2C.MASTER)
    
    # Define periods for each task
    period_0 = .02
    
    period_1 = .01
    
    ## @brief   Touch panel driver object
    #
    touch_driver = touch_panel.Touch_Panel(xm, xp, ym, yp, 100, 176, 50, 88)
    
    ## @brief   BNO055 (IMU) driver object
    #
    IMU_driver = BNO055.BNO055(i2c)
    
    ## @brief   User Task
    #
    task_1 = task_user.Task_User(period_0, balanceCMD, dataCMD)

    ## @brief   Touch panel task
    #
    task_2 = task_touch.Task_Touch(period_0, x, x_dot, y, y_dot, touch_driver)
    
    ## @brief   IMU task
    #
    task_3 = task_IMU.Task_IMU(period_0,  theta_x, theta_x_dot, theta_y, theta_y_dot, IMU_driver)
    
    ## @brief   Motor control task
    #
    task_4 = task_control.Task_Control(period_1, x, x_dot, y, y_dot, theta_x, theta_x_dot, theta_y, theta_y_dot, motor_1, motor_2, balanceCMD)
    
    ## @brief   Data collection task
    #
    task_5 = task_data.Task_Data(period_0, x, x_dot, y, y_dot, theta_x, theta_x_dot, theta_y, theta_y_dot, dataCMD)
    
    
    
    
    # A list of tasks to run (**)
    task_list = [task_1, task_2, task_3, task_4, task_5]



    while (True):
        try:
            for task in task_list:
                
                task.run()
            
        except KeyboardInterrupt:
            
            break

    print('Program Terminating')


if __name__=='__main__':
    
    main()