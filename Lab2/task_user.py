'''@file                    task_user.py
   @brief                   Constructs the finite state machine for user_task
   @details                 User_task includes three functions: one to initialize appropriate
                            variables, one to carry out appropriate actions within each state
                            and the proper movement between them, and one to
                            carry out the transition between each of the states. The final function
                            is not necessary, but makes the code more readable and user-friendly.
                            See description of the self.transition_to function for more details.
   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    October 19, 2021
'''

import pyb, utime
import time
from micropython import const

S0_INIT                 = const(0)
S1_WAIT_FOR_INPUT       = const(1)
S2_ZERO_POS             = const(2)
S3_PRINT_POS            = const(3)
S4_PRINT_DELTA          = const(4)
S5_COLLECT              = const(5)
S6_END_COLLECT          = const(6)

class Task_User:
    ''' @brief              Defines the Task_User class
        @details            The class contains three functions: init, run, and transition_to.
                            The class defines appropriate variables and maps the prompts and
                            actions required by the task_user finite state machine.
    '''

    # Define the constructor
    def __init__(self, period, enc_pos, delta, flag):
        ''' @brief          Constructs an user task object
            @details        The init function initializes the state machine to the S0 INIT state,
                            initializes runs of the state machine to zero, and defines the serial
                            import so that the file can read user imports from the keyboard to
                            transition between states.
        '''

        ## The period of the task (in ms)
        self.period = period

        ## A shares.Share object representing encoder position
        self.enc_pos = enc_pos

        ## A shares.Share object representing change in encoder position
        self.delta = delta

        ## The state to run in the finte state machine
        self.state = S0_INIT

        ## Number of runs of the finite state machine
        self.runs = 0

        ## Serial port for user input
        self.serport = pyb.USB_VCP()

        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
        self.time_ = 0
        
        self.time_step = 1
        
        self.flag = flag


    def run(self):
        ''' @brief          Constructs the flow of the user_task finite state machine
            @details        The user_task finite state machine has seven states: S0-S6.
                            This function runs each of these states and defines the proper
                            transitions between them according to the state diagram.
                            The serial import variable defined in the init function is utilized
                            so that user inputs can be interpreted and prompt the function to
                            transition between the states.
                            S0: INIT
                            S1: WAIT FOR INPUT
                            S2: ZERO POSITION
                            S3: PRINT POSITION
                            S4: PRINT DELTA
                            S5: COLLECT DATA FOR 30 SECONDS AND PRINT RESULTS
                            S6: END DATA COLLECTION PREMATURELY
        '''
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if (self.state==0):
                print('Welcome. Initial position of the encoder is zero.')
                print('Choose from list of input options: ')
                print('-To print the position of the encoder, press "p"')
                print('-To print delta between the last two positions, press "d"')
                print('-To re-zero the position of the encoder, press "z"')
                print('-To collect position data for 30 seconds, press "g"')
                print('-To pre-emptively terminate data collection, press "s"')
                print('-Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)

            elif (self.state==1):
                if (self.serport.any()):
                    self.prompt = self.serport.read(1)
                    if self.prompt == b'z' or self.prompt == b'Z':
                        self.transition_to(S2_ZERO_POS)
                    elif self.prompt == b'p' or self.prompt == b'P':
                        self.transition_to(S3_PRINT_POS)
                    elif self.prompt == b'd' or self.prompt == b'D':
                        self.transition_to(S4_PRINT_DELTA)
                    elif self.prompt == b'g' or self.prompt == b'G':
                        self.transition_to(S5_COLLECT)
                    else:
                        print('Command \'{:}\' is invalid.'.format(self.prompt))
                        print('Waiting for user input: ')

            elif (self.state==2):
                self.flag.write(1)
                print('Encoder position zeroed.')
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)

            elif (self.state==3):
                print('Position is: {:}'.format(self.enc_pos.read()))
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)

            elif (self.state==4):
                print('Delta is: {:3d}'.format(self.delta.read()))
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)

            elif (self.state==5):
                
                try:
                    collection = []
                    
                    print('Collecting data for 30')
                    
                    for idx in range(0,30):
                            
                        if (self.serport.any()):
                            self.prompt = self.serport.read(1)
                            
                            if self.prompt == b's' or self.prompt == b'S':
                                
                                self.transition_to(S6_END_COLLECT)
                                
                                break
                            
                            else:
                        
                                collection.append('{:},     {:}'.format(idx, self.enc_pos.read()))
                            
                                time.sleep(self.time_step)
                            
                    print('Waiting for user input: ')
                    
                    pass
                    
                    
                except:
                    
                    for idx in range(0,30):
                        
                        print(collection(idx))
                
                    self.transition_to(S1_WAIT_FOR_INPUT)
                            
              #  while self.prompt!=b's' or self.prompt!=b'S':
                    
                #    for idx in range(0,30):
                        
                   #     if (self.serport.any()):
                   #         self.prompt = self.serport.read(1)
                            
                    #        if self.prompt == b's' or self.prompt == b'S':
                           #     self.transition_to(S6_END_COLLECT)
                            
                    #    print('{:},      {:},'.format(self.time_,self.enc_pos.read()))
                        
                    #    self.time_+=self.time_step
                        
                    #    time.sleep(self.time_step)
                    
                  #  print('Waiting for user input: ')
                  #  break 
                
             #   self.transition_to(S1_WAIT_FOR_INPUT)


            elif (self.state==6):
                print('Data collected ended prematurely.')
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)

            else:
                raise ValueError('Invalid State')

            self.next_time = utime.ticks_add(self.next_time, self.period)
            self.runs += 1

    def transition_to(self, new_state):
        ''' @brief          Transitions the machine from one state to the next
            @details        This function allows for better readability of the code.
                            Rather than prompting the next state with "self.state==1",
                            This function allows the code to be "self.transition_to(__)".
                            State values were initialized with names so that each state's
                            function is indicated in the code as well. Rather than
                            "self.transition_to(1)", the code displays
                            "self.transition_to(S1_WAIT_FOR_INPUT)".
            @param new_state    An integer value indicating the next state
        '''
        self.state = new_state


