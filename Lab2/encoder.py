'''@file                    encoder.py
   @brief                   Constructs the encoder driver class
   @details                 The class includes five functions: init, update,
                            get_position, set_position, and get_delta. Position and delta values
                            are obtained using these functions, and the information is shared
                            to the task_encoder class by utilizing a "shares" class. See
                            details for each function within the class for more information.
   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    October 19, 2021
'''
import time
import pyb

class Encoder:
    ''' @brief              Constructs an encoder class
        @detials            The class contains five functions: init, update, get_position,
                            set_position, and get_delta. See details for each function within
                            the class for more information.
    '''

    def __init__(self, tim4, pinB6, pinB7):
        ''' @brief          Constructs an encoder object
            @details        This function assigns and initializes proper variables in order
                            for position and delta information to be calculated and updated.
                            This includes assigning proper pins and timers associated with
                            the encoder, and initializing the encoder position and delta values
                            to zero.
        '''
        
        self.pinB6 = pinB6
        self.pinB7 = pinB7
        self.tim4 = tim4
        self.t4ch1 = tim4.channel(1, tim4.ENC_A, pin=pinB6)
        self.t4ch2 = tim4.channel(2, tim4.ENC_AB, pin=pinB7)
        
        self.pos1 = 0
        self.enc_pos = 0
        self.delta = 0
        
        self.reset_value = 0


    def update(self):
        ''' @brief          Updates encoder position and delta
            @details        This function calculates the current position and
                            delta (change in position) values for the encoder using
                            information from the nucleo. The encoder "overflows" when the
                            position goes below zero or surpasses 635535 tics. The function
                            accounts for this overflow, allowing encoder position to exceed
                            635535 and pass below zero. This function is run every 0.002
                            seconds to allow for up-to-date information on encoder postion
                            when the user interacts with the program.
        '''
       
        pos2 = self.tim4.counter() - self.reset_value

        delta = pos2 - self.pos1
                
        if delta > 32767.5:
                    delta = delta - 65535
                
        elif delta < -32767.5:
                    delta = delta + 65535
                    
        self.pos1 = pos2

        ##new position of the encoder
        #
        self.enc_pos += delta

        ##change in position since last tracked position
        #
        self.delta = delta


    def get_position(self):
        ''' @brief          Returns encoder position
            @details        This function does not perform any calculations- it only
                            returns the current encoder position. This function is
                            utilized by the share class for proper communication of
                            information within the program
            @return         Current encoder position
        '''
        return self.enc_pos

    def set_position(self):
        ''' @brief          Sets encoder position
            @details        This function is utilized when the user wishes to reset the
                            encoder position to zero. Encoder position and delta values
                            are reset to zero.
            @return         Returns zero value for encoder position
        '''
        self.enc_pos = 0
        self.delta = 0
        self.pos1 = 0
        
        self.reset_value = self.tim4.counter()
        
        return 0
        #resets the position to a specified value (I'm assuming we'll want this to be zero).
        #make sure variable changes are accounted for so that update() still works
        #print('Setting position and delta values')

    def get_delta(self):
        ''' @brief          Returns encoder delta
            @details        This function does not perform any calculations- it only
                            returns the current change in encoder position. This function is
                            utilized by the share class for proper communication of
                            information within the program
            @return         Current delta value (current position - previous position)
        '''
        return self.delta