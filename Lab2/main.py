'''@file                    main.py
   @brief                   Main function file for the program
   @details                 This file interacts with all other class files in the program:
                            The encoder driver, encoder task class, user task class, and
                            shares class. The main program defines the tasks and runs through
                            them in a continuous loop, while managing the sharing of position
                            and delta information between the classes.
   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    October 12, 2021
'''
# task encoder feeds task user information

import time
import encoder
import task_encoder
import task_user
import shares
import pyb

## A task object from the module "encoder.py"
# from the class "Encoder"

def main():
    ''' @brief              Main function for the program
        @details            This function defines the variables of shared information for the program
                            (position and delta), and defines the encoder object, the encoder task,
                            and the user task. The function puts the encoder and user tasks in a list,
                            and runs a continuous loop through these tasks until the program is
                            prompted to terminate via KeyboardInterrupt.
    '''

    ##encoder position, shared variable
    enc_pos = shares.Share(0)

    ##delta value, shared variable
    delta = shares.Share(0)

    ##prompt to zero encoder position, shared variable
    flag = shares.Share(0)
    
    ##first pin that tracks encoder position
    pinB6 = pyb.Pin(pyb.Pin.cpu.B6)

    ##second pin that tracks encoder position
    pinB7 = pyb.Pin(pyb.Pin.cpu.B7)

    ##timer that tracks encoder position
    tim4 = pyb.Timer(4, prescaler=0, period= 65535)

    ##defines encoder driver object from the encoder class
    EncoderDriverObj = encoder.Encoder(tim4, pinB6, pinB7)

    ##period for each update iteration - 2ms
    period = int(0.002)
    

    ## An encoder task object
    task_1 = task_encoder.Task_Encoder(period, delta, enc_pos, EncoderDriverObj, flag)
    
    ## A user interface object
    task_2 = task_user.Task_User(period, delta, enc_pos, flag)
    
    
    
    ## A list of tasks to run (encoder task, user task)
    task_list = [task_1, task_2]



    while (True):
        try:
            for task in task_list:
                
                task.run()
            
        except KeyboardInterrupt:
            
            break

    print('Program Terminating')


if __name__=='__main__':
    
    main()