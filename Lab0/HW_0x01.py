'''@file                    HW_0x01.py
   @brief                   This file provides an package that computes change.
   @author                  Charlie Refvem
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    April 12, 2021
   
   @package                 HW_0x01
   @brief                   This package provides an interface that computes change.
   @author                  Charlie Refvem
   @copyright               This package is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    April 12, 2021
'''

## List of denomination names and values
denoms = [['Penny',       1],
          ['Nickle',      5],
          ['Dime',       10],
          ['Quarter',    25],
          ['$1 Bill',   100],
          ['$5 Bill',   500],
          ['$10 Bill', 1000],
          ['$20 Bill', 2000]]

def getChange(price, pmt):
    '''
    @brief          Computes change for a monetary transaction.
    @details        Given the price of an item and the denominations to pay
                    with, computes the proper change using the fewest number of
                    denominations.
    @param price    The price of the item to purchase as an integer number of
                    cents.
    @param pmt      A list containing the quantity for each denomination.
                    Denominations are ordered smallest to largest: pennies, 
                    nickels, dimes, quarters, ones, fives, tens, and twenties.
    @return         If the payment is sufficient, returns a  list representing
                    the computed change in the same format as pmt. If payment
                    is insufficient, returns None.
    '''
    

    
    # Compute otal value of provided denominations
    pmtVal = sum(denomQty*denom[1] for denomQty, denom in zip(pmt,denoms))
    
    # Return early if there are insufficient funds
    if pmtVal < price:
        return None
    
    # If funds are sufficient, compute required change
    cngVal = pmtVal - price
    
    # Start an empty list to fill with denomination quantities
    cng = []
    
    # Starting with the biggest denomination, compute quantity of each.
    for denom in reversed(denoms):
        cng.insert(0, cngVal // denom[1])
        cngVal -= denom[1]*cng[0]
        
    return cng

# Testing code
#
# The following test code evaluates the performance of the getChange function
# using several test cases.
if __name__ == "__main__":
    ## A set of test cases to run while validating the function getChange().
    # The test cases will be passed into getChange() to evaluate the function.
    # The cases have the format {test_name:[price, [pmt]]}
    test_cases = {'Test 1':[2249, [0,    0, 0, 0, 0, 0, 1, 1]],
                  'Test 2':[2249, [2500, 0, 0, 0, 0, 0, 0, 0]],
                  'Test 3':[2249, [0,    0, 0, 0, 0, 0, 0, 0]],
                  'Test 4':[ 947, [1,    1, 1, 1, 1, 1, 1, 1]]}
    
    # For each test case we'll attempt to run the function and print to the 
    # console some well-formatted results
    for test_name, test_case in test_cases.items():
        # First parse through the test case to get the price and payment and
        # then run the getChange() function
        
        ## The price of the item to purchase.
        price = test_case[0]
        
        ## The list of denominations to use as payment when making a purchase.
        pmt = test_case[1]
        
        ## The change returned after the purchase is complete.
        cng = getChange(price,pmt)
        
        # Each test will have its name printed first
        print(test_name)
        
        # Then information about the price is printed
        print('  Price:\t${:.2f}'.format(price/100))
        
        ## The value of the payment is computed and printed
        pmtVal = sum(denomQty*denom[1] for denomQty, denom in zip(pmt,denoms))
        print('  Payment:\t${:.2f}'.format(pmtVal/100))
        
        # Information about the payment denominations is printed
        for denom, denomQty in zip(denoms, pmt):
            print('    Quantity {:d} of {:s}'.format(denomQty, denom[0]))
        
        # Check if the purchase was successful or if there were insufficient funds
        if cng is not None:
            
            # If funds sufficient, calculate the value of the change returned
            # and then print the value
            
            ## The value of the change returned.
            cngVal = sum(denomQty*denom[1] for denomQty, denom in zip(cng,denoms))
            print('  Change:\t${:.2f}'.format(cngVal/100))
            
            # Finally information about the change returned is printed.
            for denomQty, denom in zip(cng, denoms):
                print('    Quantity {:d} of {:s}'.format(denomQty, denom[0]))
        
        # If the funds were insufficient, display a message indicating so
        else:
            print('  Change: Insufficient Funds - No Change Returned')
        
        # Print an extra blank line at the end to separate one test case from
        # the next.
        print('')