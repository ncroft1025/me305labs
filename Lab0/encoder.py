'''@file                    encoder.py
   @brief                   Brief doc for encoder.py
   @details                 Detailed doc for encoder.py 
   @author                  Your Name
   @copyright               License Info
   @date                    January 1, 1970
'''

import pyb

class Encoder:
    '''@brief               An encoder driver object
       @details             Details
       @author              Your Name
       @copyright           License Info
       @date                January 1, 1970
    '''

    def __init__(self):
        '''@brief           Constructor for encoder driver
           @details         Detailed info on encoder driver constructor
        '''
        
        ## @brief           The position of the encoder
        #  @details         More details about the encoder
        #
        self.position = 0

    def get_position(self):
        '''@brief           Gets the encoder's position
           @details         Detailed info on encoder get_position method
           @return          The position of the encoder
        '''
        return self.position

    def set_position(self, position):
        '''@brief           Zeros out the encoder
           @details         Detailed info on encoder zero function
           @param position  The new position value to assign to the encoder's
                            present location.
        '''
        pass