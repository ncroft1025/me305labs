'''@file                    motor.py
   @brief                   Brief doc for motor.py
   @details                 Detailed doc for motor.py 
   @author                  Your Name
   @author                  A Contributor's Name
   @copyright               License Info
   @date                    January 1, 1970
'''

import pyb

class Motor:
    '''@brief               A motor driver object
       @details             Details
       @author              Your Name
       @copyright           License Info
       @date                January 1, 1970
    '''

    def __init__(self):
        '''@brief           Constructor for motor driver
           @details         Detailed info on motor driver constructor
        '''
            

    def set_duty_cycle(self,level):
        '''@brief           Sets duty cycle for motor
           @details         Detailed info on motor driver duty cycle function
           @param level     The desired duty cycle
        '''
            